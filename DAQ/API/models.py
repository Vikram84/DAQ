from django.db import models

# Create your models here.
class RTU_Connections(models.Model):
    sno = models.CharField(max_length=50)
    name = models.CharField(max_length=50)

class RTU_devices_running(models.Model):
    sno = models.CharField(max_length=50)
    device = models.CharField(max_length=50)

