from django.http import JsonResponse
from .models import RTU_Connections
import socket
# Create your views here.
def rtu_start(request):
    def start(dbdata):
        import socket
        HOST = "127.0.0.1"
        PORT = 21000
        ret = False
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            post_data = "START"
            post_data = post_data.encode()
            s.sendall(post_data)
            data = s.recv(1024)
            if data == b"STARTED":
                ret = True
        s.close()
        return ret
    db_data = RTU_Connections.objects.all()
    print(db_data, 6666666)
    db_data = [(i.sno, i.name) for i in db_data]
    print(db_data, 6666666)
    status = 'Started' if start(db_data) else 'Start Failed'
    response = {'Status': status}
    return JsonResponse(response)

def rtu_stop(request):
    def stop():
        import socket
        HOST = "127.0.0.1"  # The server's hostname or IP address
        PORT = 21000  # The port used by the server
        ret = False
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            post_data = "STOP"
            post_data = post_data.encode()
            s.sendall(post_data)
            data = s.recv(1024)
            if data == b"STOPPED":
                ret = True
        s.close()
        return ret
    status = "STOPPED" if stop() else "STOP FAILLED"
    return JsonResponse({'status':status})
