import socket, time, threading
import sqlite3

class DB:
    def __init__(self, file=r'C:\Users\All\Downloads\DAQ\DAQ\DAQ\db.sqlite3', tbl=None):
        self.db = sqlite3.connect(file)
        self.cursor = self.db.cursor()
        self.table = tbl
    def selectAll(self):
        self.data = self.cursor.execute(f"select * from {self.table}")
    def close(self):
        self.cursor.close()
        self.db.close()

class RTU(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        RTU_Connections = DB(tbl='API_rtu_connections')
        RTU_Connections.selectAll()
        self.db_data = [i for i in RTU_Connections.data]
        RTU_Connections.close()
    def run(self):
        while not KILL_THREAD:
            print(self.db_data, KILL_THREAD)
            time.sleep(2)

if __name__ == '__main__':
    HOST = "127.0.0.1"
    PORT = 21000
    KILL_THREAD = False
    data = None    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        while 1:
            conn, addr = s.accept()
            with conn:
                print(f"Connected by {addr}")
                while True:
                    data = conn.recv(1024)
                    if not data:
                        time.sleep(0.5)
                        continue
                    if data == b"STOP":
                        conn.sendall(b"STOPPED")
                        KILL_THREAD = True
                        break
                    elif data == b"START":
                        KILL_THREAD = False
                        conn.sendall(b"STARTED")
                        obj = RTU()
                        obj.start()
                        break
        